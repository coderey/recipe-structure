<?php

namespace CodereyTests\RecipeStructure\TestClasses;

use Coderey\RecipeStructure\Ingredient;
use Coderey\RecipeStructure\IngredientParser;
use Coderey\RecipeStructure\Recipe;

class BrokenIngredientParser extends IngredientParser
{
    protected static array $dictionaries = [
        Recipe::class,
    ];

    public function __construct(string $ingredientString)
    {
        parent::__construct($ingredientString);
        static::$dictionaries[] = new Ingredient();
    }
}