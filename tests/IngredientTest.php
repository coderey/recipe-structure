<?php

namespace CodereyTests\RecipeStructure;

use Coderey\RecipeStructure\Ingredient;

class IngredientTest extends TestCase
{
    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     *
     * @return void
     */
    public function testGetterAndSetter()
    {
        $ingredient = new Ingredient();
        $ingredient->setName('Onions');
        $ingredient->setAmount(505.5);
        $ingredient->setUnit('g');
        $ingredient->setAdditionalInfo('in pieces');

        $this->checkResult($ingredient, 'Onions', 505.5, 'g', 'in pieces');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithSpaceBetweenAmountAndUnitAndInfoAfterComma()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('505.5 g Onions, in pieces');
        $this->checkResult($ingredient, 'Onions', 505.5, 'g', 'in pieces');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithSpaceBetweenAmountAndUnitAndWithoutInfo()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('505.5 g Onions');
        $this->checkResult($ingredient, 'Onions', 505.5, 'g', '');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithoutSpaceBetweenAmountAndUnitAndInfoAfterComma()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('505.5g Onions, in pieces');
        $this->checkResult($ingredient, 'Onions', 505.5, 'g', 'in pieces');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithoutSpaceBetweenAmountAndUnitAndInfoInBrackets()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('505.5g Onions (in pieces)');
        $this->checkResult($ingredient, 'Onions', 505.5, 'g', 'in pieces');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithoutSpaceBetweenAmountAndUnitAndInfoInSquareBrackets()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('505.5g Onions [in pieces]');
        $this->checkResult($ingredient, 'Onions', 505.5, 'g', 'in pieces');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithoutAnyAmountOrUnitOrInfo()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('some Onions');
        $this->checkResult($ingredient, 'some Onions', 0.0, '', '');
    }

    /**
     * @covers \Coderey\RecipeStructure\Ingredient
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @return void
     */
    public function testUsingIngredientParserWithoutAnyAmountOrUnitWillNotSplitBetweenNameAndInfo()
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient('some Onions, in pieces');
        $this->checkResult($ingredient, 'some Onions, in pieces', 0.0, '', '');
    }

    protected function checkResult(Ingredient $ingredient, $name, $amount, $unit, $info)
    {
        $array = $ingredient->toArray();
        $json  = $ingredient->toJson();

        $this->assertEquals($name, $ingredient->getName());
        $this->assertEquals($amount, $ingredient->getAmount());
        $this->assertEquals($unit, $ingredient->getUnit());
        $this->assertEquals($info, $ingredient->getAdditionalInfo());

        $this->assertArrayHasKey('name', $array);
        $this->assertEquals($name, $array['name']);
        $this->assertStringContainsString('"name":"' . $name . '"', $json);

        $this->assertArrayHasKey('unit', $array);
        $this->assertEquals($unit, $array['unit']);
        $this->assertStringContainsString('"unit":"' . $unit . '"', $json);

        $this->assertArrayHasKey('amount', $array);
        $this->assertEquals($amount, $array['amount']);
        $this->assertStringContainsString('"amount":' . $amount, $json);

        $this->assertArrayHasKey('info', $array);
        $this->assertEquals($info, $array['info']);
        $this->assertStringContainsString('"info":"' . $info . '"', $json);
    }
}