<?php

namespace CodereyTests\RecipeStructure;

use Coderey\RecipeStructure\IngredientParser;
use Coderey\RecipeStructure\UnitDictionaries\German;
use Coderey\RecipeStructure\UnitDictionaries\UnitDictionaryInterface;
use CodereyTests\RecipeStructure\TestClasses\BrokenIngredientParser;
use Mockery;

class IngredientParserTest extends TestCase
{
    /**
     * @covers \Coderey\RecipeStructure\IngredientParser
     * @covers \Coderey\RecipeStructure\UnitDictionaries\German
     *
     * @return void
     */
    public function testAdditionalDictionaryWillBeUsedWhenLoaded()
    {
        //at first try a german unit
        $parser = new IngredientParser('100 Becher Funny Things, really funny');
        $this->assertEquals(100, $parser->getAmount());
        $this->assertEquals('Becher', $parser->getUnit());
        $this->assertEquals('Funny Things, really funny', $parser->getIngredientName());

        //now try a non-existing unit - it should be handled as name
        $parser = new IngredientParser('100 foo Funny Things, really funny');
        $this->assertEquals(100, $parser->getAmount());
        $this->assertEquals('', $parser->getUnit());
        $this->assertEquals('foo Funny Things, really funny', $parser->getIngredientName());

        //now we load the new dictionary...
        $mock = Mockery::mock(UnitDictionaryInterface::class);
        $mock->shouldReceive('getUnifiedUnitArray')->andReturn(['foo' => 'bar']);
        /** @var UnitDictionaryInterface $mock */
        IngredientParser::addDictionary($mock);

        //german unit should still work
        $parser = new IngredientParser('100 Becher Funny Things, really funny');
        $this->assertEquals(100, $parser->getAmount());
        $this->assertEquals('Becher', $parser->getUnit());
        $this->assertEquals('Funny Things, really funny', $parser->getIngredientName());

        //now try our NEW-existing unit "foo" - it should return the unified unit "bar"
        $parser = new IngredientParser('100 foo Funny Things, really funny');
        $this->assertEquals(100, $parser->getAmount());
        $this->assertEquals('foo', $parser->getUnit());
        $this->assertEquals('bar', $parser->getUnitUnified());
        $this->assertEquals('Funny Things, really funny', $parser->getIngredientName());
    }

    public function amountDataProvider(): array
    {
        return [
            ['½', 0.5],
            ['⅓', 0.333333],
            ['⅔', 0.666667],
            ['¼', 0.25],
            ['¾', 0.75],
            ['⅕', 0.2],
            ['⅖', 0.4],
            ['⅗', 0.6],
            ['⅘', 0.8],
            ['⅙', 0.166667],
            ['⅚', 0.833333],
            ['⅐', 0.142857],
            ['⅛', 0.125],
            ['⅜', 0.375],
            ['⅝', 0.625],
            ['⅞', 0.875],
            ['⅑', 0.111111],
            ['⅒', 0.1],
        ];
    }

    /**
     * @covers \Coderey\RecipeStructure\IngredientParser
     *
     * @dataProvider amountDataProvider
     *
     * @param string|int|float $amount
     * @param float $expectedAmount
     *
     * @return void
     */
    public function testFractionValuesWillBeTranslatedToFloat(string|int|float $amount, float $expectedAmount)
    {
        $parser = new IngredientParser($amount . 'l Milch');
        $this->assertEqualsWithDelta($expectedAmount, $parser->getAmount(), 0.000001);
        $this->assertEquals('l', $parser->getUnit());
        $this->assertEquals('Milch', $parser->getIngredientName());
    }

    public function testInvalidDictionariesWillBeSkipped()
    {
        $parser = new BrokenIngredientParser('500g Mehl');

        $this->assertEquals(500, $parser->getAmount());
        $this->assertEquals('', $parser->getUnit());
        $this->assertEquals('g Mehl', $parser->getIngredientName());


        BrokenIngredientParser::addDictionary(new German());
        $parser = new BrokenIngredientParser('500g Mehl');

        $this->assertEquals(500, $parser->getAmount());
        $this->assertEquals('g', $parser->getUnit());
        $this->assertEquals('Mehl', $parser->getIngredientName());
    }

}