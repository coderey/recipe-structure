<?php

namespace CodereyTests\RecipeStructure;

use Coderey\RecipeStructure\Ingredient;
use Coderey\RecipeStructure\IngredientsCollection;
use Coderey\RecipeStructure\Recipe;

class IngredientsCollectionTest extends TestCase
{
    /**
     * @covers \Coderey\RecipeStructure\IngredientsCollection
     *
     * @return void
     */
    public function testIngredientsWillBeParsedAndSameIngredientsWillBeCombined()
    {
        $collection = new IngredientsCollection();
        $collection->addIngredient('500g Mehl');
        $collection->addIngredient('400g Zucker, kann auch Rohrzucker sein');
        $collection->addIngredient('250g Mehl');

        $this->assertCount(2, $collection);

        $amounts = [];

        foreach ($collection as $item) {
            $this->assertInstanceOf(Ingredient::class, $item);
            $this->assertEquals('g', $item->getUnit());
            $name = $item->getName();
            $this->assertContains($name, ['Mehl', 'Zucker']);
            //because of every ingredient should stay only 1 time in collection - the ingredient should not yet be in amounts-array
            $this->assertArrayNotHasKey($name, $amounts);
            $amounts[$name] = $item->getAmount();
        }

        $this->assertEquals(750.0, $amounts['Mehl']);
        $this->assertEquals(400.0, $amounts['Zucker']);
    }

    /**
     * @covers \Coderey\RecipeStructure\IngredientsCollection
     *
     * @return void
     */
    public function testSameIngredientsButWithUncomparableUnitsWillNotBeCombined()
    {
        $collection = new IngredientsCollection();
        $collection->addIngredient('250g Sahne');
        $collection->addIngredient('1 Becher Sahne');

        $this->assertCount(2, $collection);

        $amounts = [];

        foreach ($collection as $item) {
            $this->assertInstanceOf(Ingredient::class, $item);
            $this->assertEquals('Sahne', $item->getName());
            $unit = $item->getUnit();
            $this->assertContains($unit, ['g', 'Becher']);
            $this->assertArrayNotHasKey($unit, $amounts);
            $amounts[$unit] = $item->getAmount();
        }

        $this->assertEquals(250.0, $amounts['g']);
        $this->assertEquals(1.0, $amounts['Becher']);
    }


    /**
     * @covers \Coderey\RecipeStructure\IngredientsCollection
     *
     * @return void
     */
    public function testOnMergingCollectionsSameIngredientsWithComparableUnitsWillBeCombined()
    {
        $collection1 = new IngredientsCollection();
        $collection1->addIngredient('500g Mehl');
        $collection1->addIngredient('200g Zucker');
        $collection1->addIngredient('250g Sahne');

        $collection2 = new IngredientsCollection();
        $collection2->addIngredient('1 Becher Sahne');
        $collection2->addIngredient('300g Mehl');
        $collection2->addIngredient('100g Rohrzucker');

        $collection3 = new IngredientsCollection();
        $collection3->mergeIngredients($collection1);
        $collection3->mergeIngredients($collection2);
        $this->assertCount(5, $collection3);

        $items = [];
        foreach ($collection3 as $item) {
            $this->assertInstanceOf(Ingredient::class, $item);
            $items[] = $item->getAmount() . ' ' . $item->getUnit() . ' ' . $item->getName();
        }

        $this->assertCount(5, $items);
        $this->assertContains('800 g Mehl', $items);
        $this->assertContains('200 g Zucker', $items);
        $this->assertContains('100 g Rohrzucker', $items);
        $this->assertContains('250 g Sahne', $items);
        $this->assertContains('1 Becher Sahne', $items);
    }

    public function nonIngredientAddMethodInputsThatShouldFailDataProvider(): array
    {
        return [
            ['1 Becher Sahne'],
            [new IngredientsCollection()],
            [new Recipe()],
            [0],
            [null],
        ];
    }

    /**
     * @covers \Coderey\RecipeStructure\IngredientsCollection
     * @dataProvider nonIngredientAddMethodInputsThatShouldFailDataProvider
     *
     * @param mixed $failingInput
     *
     * @return void
     */
    public function testAddingNonIngredientDataWillFail(mixed $failingInput): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('IngredientsCollection can only add Ingredients!');
        $collection = new IngredientsCollection();
        $collection->add($failingInput);
    }
}