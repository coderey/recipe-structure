<?php

namespace CodereyTests\RecipeStructure;

use Coderey\RecipeStructure\Ingredient;
use Coderey\RecipeStructure\IngredientsCollection;
use Coderey\RecipeStructure\Recipe;
use Coderey\RecipeStructure\RecipeInterface;

class RecipeTest extends TestCase
{
    /**
     * @return array
     */
    public function getterSetterDataProvider(): array
    {
        return [
            ['Title', 'Test Recipe'],
            ['Origin', 'phpunit test'],
            ['Difficulty', RecipeInterface::DIFFICULTY_MEDIUM],
            ['Difficulty', RecipeInterface::DIFFICULTY_MEDIUM],
            ['WorkingTime', 10],
            ['CookingTime', 15],
            ['CoolingTime', 20],
            ['TotalTime',  30],
        ];
    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @dataProvider getterSetterDataProvider
     *
     * @param string $item
     * @param mixed $value
     *
     * @return void
     */
    public function testSetterAndGetterAndCheckIfTheyAreFlaggingRecipeAsTouched(string $item, mixed $value)
    {
        $setter = 'set' . $item;
        $getter = 'get' . $item;
        $recipe = new Recipe();
        $this->assertFalse($recipe->isTouched());
        $recipe->$setter($value);
        $this->assertTrue($recipe->isTouched());
        $this->assertEquals($value, $recipe->$getter());
    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @return void
     */
    public function testSetAndGetTimes()
    {
        $recipe = new Recipe();
        //total time will be calculated if not explicit set
        $recipe->setWorkingTime(10);
        $recipe->setCookingTime(5);
        $recipe->setCoolingTime(15);

        $this->assertEquals(10, $recipe->getWorkingTime());
        $this->assertEquals(5, $recipe->getCookingTime());
        $this->assertEquals(15, $recipe->getCoolingTime());
        $this->assertEquals(30, $recipe->getTotalTime());

        $recipe->setWorkingTime(13);
        $recipe->setCookingTime(10);

        $this->assertEquals(13, $recipe->getWorkingTime());
        $this->assertEquals(10, $recipe->getCookingTime());
        $this->assertEquals(15, $recipe->getCoolingTime());
        $this->assertEquals(38, $recipe->getTotalTime());

        //but when it is set - the other values do not matter anymore

        $recipe->setTotalTime(60);

        $this->assertEquals(13, $recipe->getWorkingTime());
        $this->assertEquals(10, $recipe->getCookingTime());
        $this->assertEquals(15, $recipe->getCoolingTime());
        $this->assertEquals(60, $recipe->getTotalTime());

        $recipe->setWorkingTime(20);

        $this->assertEquals(20, $recipe->getWorkingTime());
        $this->assertEquals(10, $recipe->getCookingTime());
        $this->assertEquals(15, $recipe->getCoolingTime());
        $this->assertEquals(60, $recipe->getTotalTime());

    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @return void
     */
    public function testSetAndGetCategories()
    {
        $recipe = new Recipe();
        $this->assertFalse($recipe->isTouched());

        $recipe->addCategory('Foo');
        $this->assertTrue($recipe->isTouched());
        $this->assertEquals(['Foo'], $recipe->getCategories());

        $recipe->addCategory('Bar');
        $this->assertEquals(['Foo', 'Bar'], $recipe->getCategories());
    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @return void
     */
    public function testSetAndGetImages()
    {
        $recipe = new Recipe();
        $this->assertFalse($recipe->isTouched());

        $recipe->addImage('Foo');
        $this->assertTrue($recipe->isTouched());
        $this->assertEquals(['Foo'], $recipe->getImages());

        $recipe->addImage('Bar');
        $this->assertEquals(['Foo', 'Bar'], $recipe->getImages());
    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @return void
     */
    public function testSetAndGetInstructions()
    {
        $recipe = new Recipe();
        $this->assertFalse($recipe->isTouched());

        $recipe->addInstruction('Foo');
        $this->assertTrue($recipe->isTouched());
        $this->assertEquals(['Foo'], $recipe->getInstructions());

        $recipe->addInstruction('Bar');
        $this->assertEquals(['Foo', 'Bar'], $recipe->getInstructions());
    }

    /**
     * @covers \Coderey\RecipeStructure\Recipe
     * @return void
     */
    public function testSetAndGetIngredients()
    {
        $recipe = new Recipe();
        $this->assertFalse($recipe->isTouched());
        $recipe->addIngredient('500g Mehl');
        $this->assertTrue($recipe->isTouched());
        $this->checkIngredientsCollection($recipe, ['500 g Mehl']);

        $recipe->addIngredient('300g Zucker');
        $this->checkIngredientsCollection($recipe, ['500 g Mehl', '300 g Zucker']);

        $recipe->addIngredient('200g Mehl');
        $this->checkIngredientsCollection($recipe, ['700 g Mehl', '300 g Zucker']);

        $collection = new IngredientsCollection();
        $collection->addIngredient('300g Mehl');
        $collection->addIngredient('1 Becher Sahne');

        $recipe->setIngredients($collection);

        $this->checkIngredientsCollection($recipe, ['300 g Mehl', '1 Becher Sahne']);

        $ingredient = new Ingredient();
        $ingredient->parseIngredient('150g Zucker');
        $recipe->addIngredientObject($ingredient);

        $this->checkIngredientsCollection($recipe, ['300 g Mehl', '1 Becher Sahne', '150 g Zucker']);

        $ingredient = new Ingredient();
        $ingredient->parseIngredient('150g Mehl');
        $recipe->addIngredientObject($ingredient);

        $this->checkIngredientsCollection($recipe, ['450 g Mehl', '1 Becher Sahne', '150 g Zucker']);
    }

    /**
     * @param Recipe $recipe
     * @param array $expectedIngredients
     *
     * @return void
     */
    protected function checkIngredientsCollection(Recipe $recipe, array $expectedIngredients)
    {
        $collection = $recipe->getIngredients();
        $this->assertInstanceOf(IngredientsCollection::class, $collection);
        $this->assertCount(count($expectedIngredients), $collection);

        $ingredients = [];
        foreach ($collection as $ingredient) {
            $this->assertInstanceOf(Ingredient::class, $ingredient);
            $ingredients[] = $ingredient->getAmount() . ' ' . $ingredient->getUnit() . ' ' . $ingredient->getName();

        }

        sort($ingredients);
        sort($expectedIngredients);
        $this->assertEquals($expectedIngredients, $ingredients);
    }

}