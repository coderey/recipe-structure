# Recipe Structure

This project contains the structure of a recipe, a ingredient-parser (that identifies amount, unit, name and extra info) and a ingredient-collection that can combine similar ingredient-entries (just incrementing the amount)

These structures and parsers will be used for a recipe-parser project