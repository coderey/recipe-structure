<?php

namespace Coderey\RecipeStructure;

class Recipe implements RecipeInterface
{
    protected string                $title        = '';
    protected string                $origin       = '';
    protected array                 $instructions = [];
    protected int                   $workingTime  = 0;
    protected int                   $cookingTime  = 0;
    protected int                   $coolingTime  = 0;
    protected int                   $totalTime    = 0;
    protected int                   $difficulty   = 0;
    protected array                 $categories   = [];
    protected array                 $images       = [];
    protected bool                  $touched      = false;
    protected IngredientsCollection $ingredients;

    public function __construct()
    {
        $this->ingredients = new IngredientsCollection();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Recipe
     */
    public function setTitle(string $title): Recipe
    {
        $this->touched = true;
        $this->title   = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     * @return Recipe
     */
    public function setOrigin(string $origin): Recipe
    {
        $this->touched = true;
        $this->origin  = $origin;

        return $this;
    }

    /**
     * @return array
     */
    public function getInstructions(): array
    {
        return $this->instructions;
    }

    /**
     * @param string $instruction
     * @return Recipe
     */
    public function addInstruction(string $instruction): Recipe
    {
        $this->touched        = true;
        $this->instructions[] = $instruction;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkingTime(): int
    {
        return $this->workingTime;
    }

    /**
     * @param int $workingTime
     * @return Recipe
     */
    public function setWorkingTime(int $workingTime): Recipe
    {
        $this->touched     = true;
        $this->workingTime = $workingTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCookingTime(): int
    {
        return $this->cookingTime;
    }

    /**
     * @param int $cookingTime
     * @return Recipe
     */
    public function setCookingTime(int $cookingTime): Recipe
    {
        $this->touched     = true;
        $this->cookingTime = $cookingTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCoolingTime(): int
    {
        return $this->coolingTime;
    }

    /**
     * @param int $coolingTime
     * @return Recipe
     */
    public function setCoolingTime(int $coolingTime): Recipe
    {
        $this->touched     = true;
        $this->coolingTime = $coolingTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalTime(): int
    {
        if ($this->totalTime == 0) {
            return ($this->workingTime + $this->cookingTime + $this->coolingTime);
        }
        return $this->totalTime;
    }

    /**
     * @param int $totalTime
     * @return Recipe
     */
    public function setTotalTime(int $totalTime): Recipe
    {
        $this->touched   = true;
        $this->totalTime = $totalTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    /**
     * @param int $difficulty
     * @return Recipe
     */
    public function setDifficulty(int $difficulty): Recipe
    {
        $this->touched    = true;
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * @return IngredientsCollection
     */
    public function getIngredients(): IngredientsCollection
    {
        return $this->ingredients;
    }

    /**
     * @param IngredientsCollection $ingredients
     * @return Recipe
     */
    public function setIngredients(IngredientsCollection $ingredients): Recipe
    {
        $this->touched     = true;
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string $category
     *
     * @return RecipeInterface
     */
    public function addCategory(string $category): RecipeInterface
    {
        $this->touched = true;

        if (!in_array($category, $this->categories)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param string $image
     *
     * @return RecipeInterface
     */
    public function addImage(string $image): RecipeInterface
    {
        $this->touched = true;
        if (!in_array($image, $this->images)) {
            $this->images[] = $image;
        }

        return $this;
    }

    /**
     * @param string $ingredient
     *
     * @return RecipeInterface
     */
    public function addIngredient(string $ingredient): RecipeInterface
    {
        $this->touched = true;
        $this->ingredients->addIngredient($ingredient);

        return $this;
    }

    /**
     * @param Ingredient $ingredient
     *
     * @return RecipeInterface
     */
    public function addIngredientObject(Ingredient $ingredient): RecipeInterface
    {
        $this->touched = true;
        $this->ingredients->add($ingredient);

        return $this;
    }

    /**
     * @return bool
     */
    public function isTouched(): bool
    {
        return $this->touched;
    }
}
