<?php

namespace Coderey\RecipeStructure\UnitDictionaries;

class German implements UnitDictionaryInterface
{
    public function getUnifiedUnitArray(): array
    {
        return [
            'l'          => 'l',
            'Liter'      => 'l',
            'ltr'        => 'l',
            'cl'         => 'cl',
            'ml'         => 'ml',
            'mg'         => 'mg',
            'g'          => 'g',
            'gramm'      => 'g',
            'kg'         => 'kg',
            'Kilo'       => 'kg',
            'Pck'        => 'Pck',
            'Pck.'       => 'Pck',
            'Päckchen'   => 'Pck',
            'Prise'      => 'Prise(n)',
            'Prisen'     => 'Prise(n)',
            'Prise(n)'   => 'Prise(n)',
            'Prise/n'    => 'Prise(n)',
            'TL'         => 'TL',
            'EL'         => 'EL',
            'Wf'         => 'Würfel',
            'Würfel'     => 'Würfel',
            'Stk'        => 'Stück',
            'Stk.'       => 'Stück',
            'Stück'      => 'Stück',
            'Stück(e)'   => 'Stück',
            'Stück/e'    => 'Stück',
            'Becher'     => 'Becher',
            'Dose'       => 'Dose(n)',
            'Dosen'      => 'Dose(n)',
            'Dose(n)'    => 'Dose(n)',
            'Dose/n'     => 'Dose(n)',
            'Scheibe'    => 'Scheibe(n)',
            'Scheiben'   => 'Scheibe(n)',
            'Scheibe(n)' => 'Scheibe(n)',
            'Scheibe/n'  => 'Scheibe(n)',
            'Bund'       => 'Bund',
        ];
    }

}