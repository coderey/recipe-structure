<?php

namespace Coderey\RecipeStructure\UnitDictionaries;

interface UnitDictionaryInterface
{
    public function getUnifiedUnitArray(): array;
}