<?php

namespace Coderey\RecipeStructure;

interface IngredientInterface
{
    public function parseIngredient(string $ingredient): self;
    public function getName(): string;
    public function getAmount(): float;
    public function getUnit(): string;
    public function getAdditionalInfo(): string;
}
