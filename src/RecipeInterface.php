<?php

namespace Coderey\RecipeStructure;

interface RecipeInterface
{
    const DIFFICULTY_EASY   = 10;
    const DIFFICULTY_NORMAL = 25;
    const DIFFICULTY_MEDIUM = 50;
    const DIFFICULTY_HARD   = 100;

    public function getTitle(): string;
    public function setTitle(string $title): RecipeInterface;
    public function getOrigin(): string;
    public function setOrigin(string $origin): RecipeInterface;
    public function getInstructions(): array;
    public function addInstruction(string $instruction): RecipeInterface;
    public function getWorkingTime(): int;
    public function setWorkingTime(int $workingTime): RecipeInterface;
    public function getCookingTime(): int;
    public function setCookingTime(int $cookingTime): RecipeInterface;
    public function getCoolingTime(): int;
    public function setCoolingTime(int $coolingTime): RecipeInterface;
    public function getTotalTime(): int;
    public function setTotalTime(int $totalTime): RecipeInterface;
    public function getDifficulty(): int;
    public function setDifficulty(int $difficulty): RecipeInterface;
    public function getIngredients(): IngredientsCollection;
    public function setIngredients(IngredientsCollection $ingredients): RecipeInterface;
    public function addIngredient(string $ingredient): RecipeInterface;
    public function getCategories(): array;
    public function addCategory(string $category): RecipeInterface;
    public function getImages(): array;
    public function addImage(string $image): RecipeInterface;

}
