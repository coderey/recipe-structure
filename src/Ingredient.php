<?php

namespace Coderey\RecipeStructure;

use Illuminate\Contracts\Support\Jsonable;
use JetBrains\PhpStorm\ArrayShape;

class Ingredient implements IngredientInterface, Jsonable
{
    protected string $name   = '';
    protected float  $amount = 0.0;
    protected string $unit   = '';
    protected string $info   = '';

    public function parseIngredient(string $ingredient): IngredientInterface
    {
        $this->name   = $ingredient;
        $parser       = new IngredientParser($ingredient);
        $this->amount = $parser->getAmount();
        $this->unit   = $parser->getUnitUnified();

        if (($this->amount != 0.0 || $this->unit != '')
            && preg_match('/^(?P<name>[^\(\[,]+)[\(\[,](?P<info>.*?)[\)\]]?$/i', $parser->getIngredientName(), $out)
        ) {
            $this->name = trim($out['name']);
            $this->info = trim($out['info']);
        } else {
            $this->name = $parser->getIngredientName();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Ingredient
     */
    public function setName(string $name): Ingredient
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Ingredient
     */
    public function setAmount(float $amount): Ingredient
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     * @return Ingredient
     */
    public function setUnit(string $unit): Ingredient
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return Ingredient
     */
    public function setAdditionalInfo(string $info): Ingredient
    {
        $this->info = $info;

        return $this;
    }

    #[ArrayShape(['name' => "string", 'amount' => "float", 'unit' => "string", 'info' => "string"])]
    public function toArray(): array
    {
        return [
            'name'   => $this->name,
            'amount' => $this->amount,
            'unit'   => $this->unit,
            'info'   => $this->info,
        ];

    }

    public function toJson($options = 0): string
    {
        return json_encode($this->toArray());
    }
}
