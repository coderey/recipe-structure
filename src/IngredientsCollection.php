<?php

namespace Coderey\RecipeStructure;

use Illuminate\Support\Collection;
use InvalidArgumentException;

class IngredientsCollection extends Collection
{
    public function addIngredient(string $ingredientString): self
    {
        $ingredient = new Ingredient();
        $ingredient->parseIngredient($ingredientString);

        $this->add($ingredient);
        return $this;
    }

    public function mergeIngredients(IngredientsCollection $collection)
    {
        foreach ($collection as $ingredient) {
            $this->add($ingredient);
        }
    }

    public function add($item): self
    {
        if (!($item instanceof Ingredient)) {
            throw new InvalidArgumentException('IngredientsCollection can only add Ingredients!');
        }
        /** @var Ingredient $item */
        $key = $item->getName() . ' in ' . $item->getUnit();
        if ($this->offsetExists($key)) {
            /** @var Ingredient $existing */
            $existing = $this->get($key);
            $existing->setAmount($existing->getAmount() + $item->getAmount());
        } else {
            $this->put($key, $item);
        }

        return $this;
    }

}
