<?php

namespace Coderey\RecipeStructure;

use Coderey\RecipeStructure\UnitDictionaries\German;
use Coderey\RecipeStructure\UnitDictionaries\UnitDictionaryInterface;

/**
 * Parse german recipe-ingredient-units
 */
class IngredientParser
{
    protected array $unitsUnified        = [];
    protected static array $dictionaries = [
        German::class,
    ];

    protected string $unit       = '';
    protected float  $amount     = 0.0;
    protected string $ingredient = '';

    /**
     * @param string $ingredientString
     */
    public function __construct(string $ingredientString)
    {
        $this->loadUnits();
        $this->parseIngredientString($ingredientString);
    }

    public static function addDictionary(UnitDictionaryInterface $dict): void
    {
        static::$dictionaries[] = $dict;
    }

    protected function loadUnits(): void
    {
        foreach (static::$dictionaries as $dict) {
            if ($dict instanceof UnitDictionaryInterface) {
                $dictionary = $dict;
            } elseif (is_string($dict) && class_exists($dict)) {
                $dictionary = new $dict();
                if (!($dictionary instanceof UnitDictionaryInterface)) {
                    continue;
                }
            } else {
                //should never ever happen
                continue;
            }

            $this->unitsUnified = array_merge($this->unitsUnified, $dictionary->getUnifiedUnitArray());
        }
    }

    /**
     * @param string $ingredientString
     *
     * @return void
     */
    protected function parseIngredientString(string $ingredientString)
    {
        $unitsArray = [];
        foreach ($this->unitsUnified as $key => $val) {
            $unitsArray[] = preg_quote($key, '/');
        }
        $units  = implode('|', $unitsArray);
        $amount = '(½|⅓|⅔|¼|¾|⅕|⅖|⅗|⅘|⅙|⅚|⅐|⅛|⅜|⅝|⅞|⅑|⅒|[\d\.,]+)';

        if (preg_match('/^' . $amount . '\s*(' . $units . ')?\s(.*?)$/i', $ingredientString, $out)) {
            $this->amount     = $this->parseAmount($out[1]);
            $this->unit       = $out[2];
            $this->ingredient = $out[3];
        } elseif (empty($unitsArray) && preg_match('/^' . $amount . '\s*(.*?)$/i', $ingredientString, $out)) {
            $this->amount     = $this->parseAmount($out[1]);
            $this->ingredient = $out[2];
        } else {
            $this->ingredient = $ingredientString;
        }
    }

    /**
     * @param string $amount
     *
     * @return float
     */
    protected function parseAmount(string $amount): float
    {
        if (preg_match('/^[½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒]/', $amount)) {
            $values = [
                '½' => 1/2,
                '⅓' => 1/3,
                '⅔' => 2/3,
                '¼' => 1/4,
                '¾' => 3/4,
                '⅕' => 1/5,
                '⅖' => 2/5,
                '⅗' => 3/5,
                '⅘' => 4/5,
                '⅙' => 1/6,
                '⅚' => 5/6,
                '⅐' => 1/7,
                '⅛' => 1/8,
                '⅜' => 3/8,
                '⅝' => 5/8,
                '⅞' => 7/8,
                '⅑' => 1/9,
                '⅒' => 1/10,
            ];

            return $values[$amount];
        } else {
            $amount = str_replace(',', '.', $amount);
            return (float)$amount;
        }
    }

    /**
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @return string
     */
    public function getUnitUnified(): string
    {
        if (array_key_exists($this->unit, $this->unitsUnified)) {
            return $this->unitsUnified[$this->unit];
        }

        return '';
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getIngredientName(): string
    {
        return $this->ingredient;
    }
}
